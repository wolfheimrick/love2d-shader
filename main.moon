export t = 0
export shader_path = "shader.glsl"
export shader

love.load = ->
    load_shader shader_path

love.draw = ->
    love.graphics.setShader shader
    love.graphics.circle "fill", 300, 300, 640, 480

love.update = (dt) ->
    t += dt
    --shader\send "time", t

love.keypressed = (key) ->
    switch key
        when "escape"
            love.event.quit()
        when "return"
            load_shader "shader.glsl"
            t = 0

export load_shader = (path) ->
    pixelcode = love.filesystem.read path
    shader = love.graphics.newShader pixelcode
